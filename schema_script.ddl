/*
Date de creation: 2024-06-08
*/

-- Table T_PRODUIT 

CREATE TABLE IF NOT EXISTS T_PRODUIT
(
    ID SERIAL PRIMARY KEY,
    reference VARCHAR(255),
    libelle VARCHAR(255),
    estDuJour BOOLEAN,
    prix FLOAT,
    quantitestock FLOAT
);