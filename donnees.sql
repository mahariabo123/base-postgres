INSERT INTO T_PRODUIT (reference, libelle, estDuJour, prix, quantitestock) VALUES
('REF001', 'Anneau Unique', TRUE, 1000000.00, 1),
('REF002', 'Epée d''Aragorn', FALSE, 1500.00, 10),
('REF003', 'Arc de Legolas', FALSE, 1200.00, 5),
('REF004', 'Hache de Gimli', FALSE, 900.00, 8),
('REF005', 'Bâton de Gandalf', FALSE, 2000.00, 3),
('REF006', 'Lembas', TRUE, 50.00, 100),
('REF007', 'Cape d''invisibilité', TRUE, 750.00, 7),
('REF008', 'Pierre de Vision de Galadriel', FALSE, 5000.00, 2),
('REF009', 'Chevalier du Gondor', FALSE, 300.00, 20),
('REF010', 'Armure d''Isildur', FALSE, 2500.00, 1),
('REF011', 'Anneau des Nains', FALSE, 20000.00, 4),
('REF012', 'Anneau des Elfes', FALSE, 30000.00, 3),
('REF013', 'Anneau des Hommes', FALSE, 40000.00, 1),
('REF014', 'Hallebarde du Rohan', FALSE, 1000.00, 6),
('REF015', 'Heaume d''or du Rohan', FALSE, 800.00, 4),
('REF016', 'Trompette de Gondor', TRUE, 600.00, 2),
('REF017', 'Epée de Boromir', FALSE, 1400.00, 3),
('REF018', 'Dague de Frodo', TRUE, 700.00, 5),
('REF019', 'Miroir de Galadriel', FALSE, 4500.00, 1),
('REF020', 'Cornemuse de Pippin', FALSE, 300.00, 10);
